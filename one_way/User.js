// myApp/api/models/User.js
// User has a pet
module.exports = {
  attributes: {
    name: {
      type: 'string'
    },
    age: {
      type: 'number'
    },
    pony:{
      model: 'pet'
    }
  }
}