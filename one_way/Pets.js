// myApp/api/models/Pet.js
// Pet belong to User
module.exports = {
  attributes: {
    name: {
      type: 'string'
    },
    color: {
      type: 'string'
    }
  }
}