// server/api/models/Comment.js


module.exports = {

  attributes: {
    poster: {
        model:'post'
    },
    content : { type: 'string' },

    date : { type: 'string' },

    creator : { type: 'string' }
  }
};
